package org.example;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:person.properties")
public class SpringConfig {

    @Bean
    public Person spongeBob(@Value("${spongeBob.name}") String name, @Value("${spongeBob.age}") int age) {
        return new Person(name, age);
    }

    @Bean
    public Person patrick(@Value("${patrick.name}") String name, @Value("${patrick.age}") int age) {
        return new Person(name, age);
    }

    @Bean
    public Person squidward(@Value("${squidward.name}") String name, @Value("${squidward.age}") int age) {
        return new Person(name, age);
    }
}
