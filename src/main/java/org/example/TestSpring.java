package org.example;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        Person spongeBob = context.getBean("spongeBob", Person.class);
        Person patrick = context.getBean("patrick", Person.class);
        Person squidward = context.getBean("squidward", Person.class);

        System.out.println(spongeBob);
        System.out.println(patrick);
        System.out.println(squidward);

        context.close();
    }
}
